#!/bin/bash

TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_DEBUG_TOKEN/sendMessage"
TEXT="Deploy status: $1"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_DEBUG_TO&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null
